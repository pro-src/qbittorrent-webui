#!/usr/bin/env node

// This is a CLI tool used to get files from qBitorrent's source repository
// It supports an optional argument of commit hash or branch name
// It has the advantage of only CRUD'ing necessary files
// If you need to fetch an older release, delete current source first
const request = require('request')
const fs = require('fs')
const path = require('path')
const tar = require('tar')
const pkg = require('../package.json')

request.debug = true

const download = request.defaults({
  headers: { 'user-agent': 'qbt-src-dl/' + pkg.version }
})

const hostname = 'api.github.com'
const pathname = '/repos/qbittorrent/qbittorrent/tarball/'
const url      = 'https://' + hostname + pathname + (process.argv.length > 2 ?
  process.argv[2] : (process.env.npm_package_config_commit || pkg.config.commit))

const root = path.join(process.env.INIT_CWD || process.cwd(), 'qbittorrent')
const include = /\/src\/(webui|icons|lang)\/.+\.(ts|css|js|html|png|gif)$/
const exclude = /\/(build-icons)\//

if (path.join(__dirname, '..', 'qbittorrent') === root) {
  throw new Error('Failed to locate project directory, aborting...')
}

fs.mkdir(root, error => {
  if (error && error.code !== 'EEXIST') throw error

  download(url).pipe(tar.extract({
    cwd: root, strict: true, strip: 1, newer: true,
    filter(path) {
      return include.test(path) && exclude.test(path) === false
    }
  }))
})
