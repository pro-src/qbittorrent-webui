# qbittorrent-webui
npm or yarn installable package that automatically downloads qBittorrent's web-ui

The npm postinstall script will attempt to download the source files to your 
project's root directory. After running `npm i qbittorrent-webui --save` you 
should find a new directory called `qbittorrent` in your project root.

This should be entirely cross-platform but has only been tested on *nix.
The examples below are not needed for the initial download.

You can manually set the package config to a specific commit

```sh
npm set qbittorrent-webui:commit ${commit} # or branch
```

Afterwards, create a npm script in your package.json

```json
{
  "scripts": {
    "update-webui": "node_modules/qbittorrent-webui/scripts/download.js"
  }
}
```

Now you can update to the commit that was previously set or specify it

```sh
npm run update-webui

# Another example with a branch or commit sha hash
npm run update-webui -- master
npm run update-webui -- ${commit}
```
